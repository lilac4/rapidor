import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  static HomeController get to => Get.put(HomeController());

  List<DataCell> dataCellList = [];

  List<String> dataTexts = [
    '10 Hrs',
    '2Hrs',
    '2Hrs',
    '1Hrs',
    '2.5Hrs',
    '2.5Hrs'
  ];

  void createDataList() {
    for (var element in dataTexts) {
      dataCellList.add(DataCell(Text(element)));
    }
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    createDataList();

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }
}
