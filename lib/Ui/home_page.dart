import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../controller/home_controller.dart';
import '../utils/custom_text.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: GetBuilder<HomeController>(
            init: HomeController(),
            builder: (controller) {
              return Scaffold(
                appBar: appBar(context),
                body: OrientationBuilder(builder: (context, orientation) {
                  final isLandscape = orientation == Orientation.landscape;
                  print(
                      'Device Orientation: ${isLandscape ? 'Landscape' : 'Portrait'}');
                  return SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          DataTable(
                              border: TableBorder.all(color: Colors.black),
                              headingRowHeight: 90,
                              dataRowHeight: 90,
                              headingRowColor: MaterialStateProperty.all(
                                  Colors.grey.shade300),
                              dataRowColor: MaterialStateProperty.all(
                                  Colors.grey.shade300),
                              columns: const [
                                DataColumn(
                                    label: Text('Total Info for the WEEK')),
                              ],
                              rows: leftHeader),
                          Expanded(
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: DataTable(
                                  border: TableBorder.all(color: Colors.black),
                                  headingRowHeight: 90,
                                  dataRowHeight: 90,
                                  dividerThickness: 2,
                                  headingRowColor: MaterialStateProperty.all(
                                      Colors.grey.shade300),
                                  columns: header,
                                  rows: dataList,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              );
            }));
  }

  PreferredSizeWidget? appBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.deepPurpleAccent,
      actions: [
        IconButton(
            onPressed: () {
              final orientation = MediaQuery.of(context).orientation;
              showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                        title: Text(
                            orientation.toString() == 'Orientation.landscape'
                                ? 'Rotate screen orientation to portrait?'
                                : 'Rotate screen orientation to landscape?'),
                        titleTextStyle:
                            const TextStyle(fontSize: 15, color: Colors.black),
                        actions: [
                          ElevatedButton(
                              onPressed: () async {
                                Get.back();
                                orientation.toString() ==
                                        'Orientation.landscape'
                                    ? SystemChrome.setPreferredOrientations(
                                        [DeviceOrientation.portraitUp])
                                    : SystemChrome.setPreferredOrientations([
                                        DeviceOrientation.landscapeLeft,
                                        DeviceOrientation.landscapeRight
                                      ]);
                              },
                              child: CustomText('yes',
                                  color: Colors.blue, fontSize: 12)),
                          TextButton(
                              onPressed: () => Get.back(),
                              child: CustomText('no',
                                  color: Colors.blue, fontSize: 12))
                        ],
                      ));
            },
            icon: const Icon(Icons.crop_rotate, color: Colors.white))
      ],
    );
  }

  List<DataRow> leftHeader = const [
    DataRow(cells: [DataCell(Text(''))]),
    DataRow(cells: [DataCell(Text('Runtime'))]),
    DataRow(cells: [DataCell(Text('Jogging Time'))]),
    DataRow(cells: [DataCell(Text('Exercise Time'))]),
    DataRow(
        cells: [DataCell(Text('Total Time\n(Running+Jogging+\nExercise)'))]),
    DataRow(cells: [
      DataCell(Text('Running\nTime engagement %\n(Running / Total Time)'))
    ]),
    DataRow(cells: [
      DataCell(Text('Jogging\nTime engagement %\n(Jogging / Total Time)'))
    ]),
    DataRow(cells: [
      DataCell(Text('Exercise\nTime engagement %\n(Exercise / Total Time)'))
    ]),
  ];

  List<DataRow> dataList = [
    DataRow(cells: [
      const DataCell(Text('')),
      DataCell(Text(DateFormat('yyyy-MM-dd')
          .format(DateTime.now())
          .replaceAll('-', '/'))),
      DataCell(Text(DateFormat('yyyy-MM-dd')
          .format(DateTime.now())
          .replaceAll('-', '/'))),
      DataCell(Text(DateFormat('yyyy-MM-dd')
          .format(DateTime.now())
          .replaceAll('-', '/'))),
      DataCell(Text(DateFormat('yyyy-MM-dd')
          .format(DateTime.now())
          .replaceAll('-', '/'))),
      DataCell(Text(DateFormat('yyyy-MM-dd')
          .format(DateTime.now())
          .replaceAll('-', '/'))),
    ], color: MaterialStateProperty.all(Colors.grey.shade300)),
    DataRow(cells: HomeController.to.dataCellList),
    DataRow(cells: HomeController.to.dataCellList),
    DataRow(cells: HomeController.to.dataCellList),
    DataRow(cells: HomeController.to.dataCellList),
    DataRow(cells: HomeController.to.dataCellList),
    DataRow(cells: HomeController.to.dataCellList),
    DataRow(cells: HomeController.to.dataCellList),
  ];

  List<DataColumn> header = const [
    DataColumn(label: Text('Total(Sun-Sat)')),
    DataColumn(label: Text('Sun')),
    DataColumn(label: Text('Mon')),
    DataColumn(label: Text('Tue')),
    DataColumn(label: Text('Wed')),
    DataColumn(label: Text('Fri')),
  ];
}
